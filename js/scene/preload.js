import Phaser from '../libs/phaser-wx.js';

export default class Preload extends Phaser.State {
  // 构造函数
  constructor(game) {
    super();
    this.game = game;
  }

  // 游戏载入前加载资源
  preload() {
    // 加载图片
    const images = {
      'background': 'src/background.jpg',
      'playBtn': 'src/ksyx.png',
      'monster': 'src/PurpleMonster.png',
      'rankBtn': 'src/hyph.png',
      'rankBackground': 'src/rankListBg.png',
      'closeBtn': 'src/X.png',
      'ground': 'src/ground.png',
			'star': 'src/star.png'
    };
    // 载入图片
		for (let name in images) {
			this.load.image(name, images[name]);
    }
    // this.game.load.audio('jumpMusic', 'src/jump.mp3');
  }

  create() {
    this.state.start('menu');
  }
}
