import Phaser from '../libs/phaser-wx.js';

export default class PlayGame extends Phaser.State {
  // 构造函数
  constructor(game) {
    super();
    this.game = game;
  }

  // 游戏载入前加载资源
  preload() {
    // 加载图片
    // this.load.audio('jumpMusic', 'src/jump.mp3');
  }

  create() {
    let bg = this.game.add.sprite(0, 0, 'background');
    bg.width = this.game.config.width * 3;
    bg.height = this.game.config.height;

    let rankBg = this.game.add.sprite(0, 0, 'rankBackground');
    rankBg.width = 600;
    rankBg.height = 940;
    rankBg.top = 667;
    rankBg.left = 375;
    rankBg.anchor.setTo(0.5, 0.5);

    this.closeBtn = this.game.add.sprite(0, 0, 'closeBtn');
    this.closeBtn.top = 237;
    this.closeBtn.left = 655;
    this.closeBtn.width = 75;
    this.closeBtn.height = 75;
    this.closeBtn.anchor.setTo(0.5, 0);

    var prevBtn = this.game.add.text(this.game.width * 0.3, this.game.height * 0.82, '上一页', {
      fontSize: '36px',
      fontWeight: 'bold',
      fill: '#A46F31'
    });
    prevBtn.anchor.setTo(0.5, 0.5);

    var nextBtn = this.game.add.text(this.game.width * 0.7, this.game.height * 0.82, '下一页', {
      fontSize: '36px',
      fontWeight: 'bold',
      fill: '#A46F31'
    });
    nextBtn.anchor.setTo(0.5, 0.5);

    this.openDataContext = wx.getOpenDataContext()
    this.sharedCanvas = this.openDataContext.canvas;
    this.sharedCanvas.width = window.pixelRatio * sysInfo.screenWidth;
    this.sharedCanvas.height = window.pixelRatio * sysInfo.screenHeight;
    this.openDataContext.postMessage({
      command: 'Rank'
    });
    this._updateSubDomainCanvas();

    this.game.input.onTap.add((position) => {
      if (window.triggerBtn(this.closeBtn, position)) {
        this.state.start('menu');
      } else if (window.triggerBtn(prevBtn, position)) {
        this.openDataContext.postMessage({
          command: 'Prev'
        });
        this._updateSubDomainCanvas();
      } else if (window.triggerBtn(nextBtn, position)) {
        this.openDataContext.postMessage({
          command: 'Next'
        });
        this._updateSubDomainCanvas();
      }
    });
  }
  _updateSubDomainCanvas() {
    this.result = this.game.add.sprite(0, 0, Phaser.XTexture(this.sharedCanvas, 0, 0, this.sharedCanvas.width, this.sharedCanvas.height));
    this.result.scale.setTo(1 / transper, 1 / transper);
  }
  update() {

  }
}
