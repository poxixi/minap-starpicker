import Phaser from '../libs/phaser-wx.js';

// 全局游戏设置
let gameOptions = {
  jumpHeight: 0,
  jumpDuration: 0,
  maxMoveSpeed: 0,
  accel: 0,
  updown: 0,
  adjust: 1,
  rightorleft: 0,
  rightorleftSpeed: 0,
  score: 0,
  clk: null,
  gameOver: 0
}

export default class PlayGame extends Phaser.State {
  // 构造函数
  constructor(game) {
    super();
    this.game = game;
    gameOptions.accel = 0.267 * 0.3 * this.game.config.height;
  }

  // 游戏载入前加载资源
  preload() {
    // 加载图片
    this.scoreMusic = new Audio('src/score.mp3');
    this.jumpMusic = new Audio('src/jump.mp3');
    // this.game.load.audio('scoreMusic', 'src/score.mp3');
    // this.game.load.audio('jumpMusic', 'src/jump.mp3');
  }

  // 加载完成
  create() {
    this.game.physics.startSystem(Phaser.Physics.ARCADE);

    let bg = this.game.add.image(0, 0, 'background');
    bg.width = this.game.config.width * 2;
    bg.height = this.game.config.height;

    let ground = this.game.add.image(0, 0, 'ground');
    ground.width = this.game.config.width;
    ground.height = this.game.config.height / 3;
    ground.top = this.game.config.height / 3 * 2;

    this.scoreLine = this.game.add.text(this.game.width * 0.5, this.game.height * 0.2, '得分：0', {
      fontSize: '60px',
      fontWeight: 'bold',
      fill: '#eaeaea'
    });
    this.scoreLine.anchor.setTo(0.5, 0.5);

    this.monster = this.game.add.sprite(0, 0, 'monster');
    this.monster.top = this.game.config.height /5 * 2;
    this.monster.left = this.game.config.width /2;
    this.monster.width = this.monster.width * 2;
    this.monster.height = this.monster.height * 2;
    this.monster.anchor.setTo(0.5, 0.5);
    this.game.physics.arcade.enable(this.monster);
    this.monster.body.colliderWroldBounds = true;

    this.stars = this.game.add.group();
    this.stars.enableBody = true;
    this.generateOneStar();

    this.game.input.onTap.add((position) => {
      if (gameOptions.gameOver === 0) {
        if (position.x * pixelRatio < 0.5 * this.game.config.width) {
          gameOptions.rightorleft = -1;
        } else {
          gameOptions.rightorleft = 1;
        }
      } else {
        if (triggerBtn(this.gameoverLine3, position)) {
          console.log('restart');
          this.state.start('play');
          gameOptions.rightorleft = 0;
          gameOptions.gameOver = 0;
          gameOptions.score = 0;
        } else if (triggerBtn(this.gameoverLine2, position)) {
          console.log('back');
          this.state.start('menu');
          gameOptions.rightorleft = 0;
          gameOptions.gameOver = 0;
          gameOptions.score = 0;
        }
      }
    });
    // this.scoreMusic = this.game.add.audio('scoreMusic');
    // this.jumpMusic = this.game.add.audio('jumpMusic');
  }

  generateOneStar() {
    clearTimeout(gameOptions.clk);
    var star = this.stars.getFirstExists(false);
    if (star) {
      star.reset(Math.random() * this.game.config.width, this.game.config.height * 0.5);
    } else {
      star = this.stars.create(Math.random() * (this.game.config.width - 30), this.game.config.height * 0.5, 'star');
    }
    gameOptions.clk = window.setTimeout(() => {
      this.gameOver();
    }, 2800);
  }
  gameOver() {
    gameOptions.gameOver = 1;
    this.gameoverLine = this.game.add.text(this.game.width * 0.5, this.game.height * 0.5, '游戏结束', {
      fontSize: '120px',
      fontWeight: 'bold',
      fill: '#dd1111'
    });
    this.gameoverLine.anchor.setTo(0.5, 0.5);

    this.gameoverLine2 = this.game.add.text(this.game.width * 0.5, this.game.height * 0.7, '回到首页', {
      fontSize: '60px',
      fontWeight: 'bold',
      fill: '#FFF'
    });
    this.gameoverLine2.anchor.setTo(0.5, 0.5);
    this.gameoverLine3 = this.game.add.text(this.game.width * 0.5, this.game.height * 0.8, '再玩一次', {
      fontSize: '60px',
      fontWeight: 'bold',
      fill: '#FFF'
    });
    this.gameoverLine3.anchor.setTo(0.5, 0.5);
    this.openDataContext = wx.getOpenDataContext();
    this.openDataContext.postMessage({
      command: 'userSet',
      data: ('' + gameOptions.score)
    });
  }
  score(monster, star) {
    this.scoreMusic.play();
    star.kill();
    this.generateOneStar();
    gameOptions.score += 1;
    this.scoreLine.text = '得分：' + gameOptions.score;
  }
  // 每帧更新
  jumping() {
    if (gameOptions.updown === 0) {
      this.monster.top += 0.5 * gameOptions.accel * 0.017 * 0.017 * gameOptions.adjust * gameOptions.adjust;
      gameOptions.adjust += 1;
      if (this.monster.bottom >= this.game.config.height / 3 * 2) {
        gameOptions.updown = 1;
        this.jumpMusic.play();
      }
    } else {
      this.monster.top -= 0.5 * gameOptions.accel * 0.017 * 0.017 * gameOptions.adjust * gameOptions.adjust;
      gameOptions.adjust -= 1;
      if (this.monster.top <= this.game.config.height/ 5 * 2 && gameOptions.updown === 1) {
        gameOptions.updown = 0;
        gameOptions.adjust = 1;
      }
    }
    if (gameOptions.rightorleft === -1 && this.monster.left >= 0) {
      gameOptions.rightorleftSpeed -= 1;
      if (gameOptions.rightorleftSpeed <= 0) {
          this.monster.left -= 0.05 * gameOptions.accel * 0.017 * 0.017 * gameOptions.rightorleftSpeed * gameOptions.rightorleftSpeed;
      } else {
          this.monster.left += 0.05 * gameOptions.accel * 0.017 * 0.017 * gameOptions.rightorleftSpeed * gameOptions.rightorleftSpeed;
      }
      // 碰到边界停下
      if (this.monster.right > this.game.config.width) {
        gameOptions.rightorleftSpeed = 0;
        this.monster.right = this.game.config.width;
      }
    } else if (gameOptions.rightorleft === 1 && this.monster.right <= this.game.config.width) {
      gameOptions.rightorleftSpeed += 1;
      if (gameOptions.rightorleftSpeed <= 0) {
          this.monster.left -= 0.05 * gameOptions.accel * 0.017 * 0.017 * gameOptions.rightorleftSpeed * gameOptions.rightorleftSpeed;
      } else {
          this.monster.left += 0.05 * gameOptions.accel * 0.017 * 0.017 * gameOptions.rightorleftSpeed * gameOptions.rightorleftSpeed;
      }
      // 碰到边界停下
      if (this.monster.left < 0) {
        gameOptions.rightorleftSpeed = 0;
        this.monster.left = 0;
      }
    } else {
      gameOptions.rightorleftSpeed = 0;
      // 碰到边界停下
      if (this.monster.right > this.game.config.width) {
        gameOptions.rightorleftSpeed = 0;
        this.monster.right = this.game.config.width;
      } else if (this.monster.left < 0) {
        gameOptions.rightorleftSpeed = 0;
        this.monster.left = 0;
      }
    }
  }
  update() {
    if (gameOptions.gameOver === 0) {
        this.jumping();
    }
    this.game.physics.arcade.overlap(this.monster, this.stars, this.score, null, this);
  }
}
