import Phaser from '../libs/phaser-wx.js';

export default class PlayGame extends Phaser.State {
  // 构造函数
  constructor(game) {
    super();
    this.game = game;
  }

  // 游戏载入前加载资源
  // preload() {
  //   // 加载图片
  //   const images = {
  //     'background': 'src/background.jpg',
  //     'playBtn': 'src/ksyx.png',
  //     'monster': 'src/PurpleMonster.png',
  //     'rankBtn': 'src/hyph.png',
  //     'rankBackground': 'src/rankListBg.png',
  //     'closeBtn': 'src/X.png',
  //     'ground': 'src/ground.png',
	// 		'star': 'src/star.png'
  //   };
  //   // 载入图片
	// 	for (let name in images) {
	// 		this.load.image(name, images[name]);
  //   }
  //   // this.game.load.audio('jumpMusic', 'src/jump.mp3');
  // }

  create() {
    let bg = this.game.add.sprite(0, 0, 'background');
    bg.width = this.game.config.width * 6;
    bg.height = this.game.config.height;

    var title = this.game.add.text(this.game.width * 0.5, this.game.height * 0.25, '星星怪兽', {
      fontSize: '90px',
      fontWeight: 'bold',
      fill: '#eaeaea'
    });
    title.anchor.setTo(0.5, 0.5);

    let playBtn = this.game.add.sprite(0, 0, 'playBtn');
    playBtn.top = this.game.config.height /5 * 4;
    playBtn.left = this.game.config.width /2;
    playBtn.width = playBtn.width;
    playBtn.height = playBtn.height;
    playBtn.anchor.setTo(0.5, 0.5);

    let rankBtn = this.game.add.sprite(0, 0, 'rankBtn');
    rankBtn.top = 200;
    rankBtn.left = this.game.config.width - 200;
    rankBtn.width = rankBtn.width;
    rankBtn.height = rankBtn.height;
    rankBtn.anchor.setTo(0.5, 0.5);

    this.game.input.onTap.add((position) => {
      if (window.triggerBtn(rankBtn, position)) {
        this.state.start('rank');
      } else if (window.triggerBtn(playBtn, position)) {
        this.state.start('play');
      }
    });

    this.monster = this.game.add.sprite(0, 0, 'monster');
    this.monster.top = this.game.config.height /5 * 2.5;
    this.monster.left = this.game.config.width /2;
    this.monster.width = this.monster.width * 3;
    this.monster.height = this.monster.height * 3;
    this.monster.anchor.setTo(0.5, 0.5);
  }
  update() {

  }
}
