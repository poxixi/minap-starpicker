import PIXI from './pixi-wx.js';
import Phaser from './phaser-wx-main.js';
import './phaser-wx-part1.js';
import './phaser-wx-part2.js';
import './phaser-wx-part3.js';
import './phaser-wx-part4.js';
import './phaser-wx-part5.js';
import './phaser-wx-part6.js';
import './phaser-wx-part7.js';
import './phaser-wx-part8.js';

Phaser.XTexture = function(xCanvas, x, y, w, h) {
  return new PIXI.Texture(new PIXI.BaseTexture(xCanvas), new PIXI.Rectangle(x, y, w, h));
};
module.exports = Phaser;
