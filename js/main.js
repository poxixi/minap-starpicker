import Phaser from 'libs/phaser-wx.js'
import Preload from 'scene/preload.js'
import Menu from 'scene/menu.js'
import Rank from 'scene/rank.js'
import Play from 'scene/play.js'

window.sysInfo = wx.getSystemInfoSync();
window.pixelRatio = sysInfo.pixelRatio;
window.transper = sysInfo.screenWidth * pixelRatio / 750;

window.triggerBtn = (btnName, position) => {
  if ((position.x * pixelRatio - btnName.left * transper) < (btnName.width * transper) && (position.x * pixelRatio - btnName.left * transper) > 0 && (position.y * pixelRatio - btnName.top * transper) < (btnName.height * transper) && (position.y * pixelRatio - btnName.top * transper) > 0) {
    return true;
  } else {
    return false;
  }
};

const game = new Phaser.Game({
  // 渲染类型
  renderer: Phaser.CANVAS,
  canvas: canvas,
  // type: Phaser.CANVAS,
  // 界面宽度，单位像素
  width: 750,
  // 界面高度
  height: 1334,
  // 背景色
  backgroundColor: 0x000000,
});

game.state.add('preload', new Preload(game));
game.state.add('menu', new Menu(game));
game.state.add('play', new Play(game));
game.state.add('rank', new Rank(game));
game.state.start('preload');

wx.showShareMenu();
wx.onShareAppMessage(() => {
  return {
    title: '天上只能有一个太阳',
    query: 'page=menu',
    imageUrl: 'https://mp.weixin.qq.com/wxopen/basicprofile?action=get_headimg&token=1926698299&t=20180815173419'
  }
});
