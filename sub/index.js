import './js/libs/weapp-adapter.js'

let __this = {};
let images = {
  'top1': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEsAAABBCAMAAACuC6m+AAABS2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDAgNzkuMTYwNDUxLCAyMDE3LzA1LzA2LTAxOjA4OjIxICAgICAgICAiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIi8+CiA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgo8P3hwYWNrZXQgZW5kPSJyIj8+LUNEtwAAAARnQU1BAACxjwv8YQUAAAABc1JHQgCuzhzpAAADAFBMVEUAAADay5jj067x3sHaypfw0LHi0qb37tzq1KXgz6Lx3sHx38Pay5bx4cjx3sTx4Mbx3b/ay5faypbbzJbx3sHby5jx38Px38Tay5ft3bzw3b3w3b3bzJjczpzx4MTZy5bw3L3t3bvay5fu373ay5fay5fdzpzby5jazJjz587w3sDx3sHv3LvZy5fx3sLw3b7l2K7w3b7w3Lvl2K/Zy5fg06Xw3bz/xRj47cz/ziv/zCb/ySH/yyT/zin/zSf/yB//xhv/yiLay5f/yB7/wxb/xxz/xBf/0C69ijv/wxX+vhD/whL9+vH47s3/zyz/wBH++e7689r9+/P/whP/1DX+zi39+Ov/1jf78tj///747s79uw//0zL/2Dz8yi3w3b779eP/1jr//Pb/0TD6xy36yC///vr0vCz8+Ov+zy/68db79N3/20L/2UD89d///vz4xTH579H99+n//fnxtSj89+fyvzfwsSfw3Lv8zC/58NP0uiv79eH+/Pf368n+0DH9zjD3wCzytyr/ziz9zS38+Oz9vhX1wDH68+X5xC34wSn7yjH3wi790DT+1Df7yzP2yDv/3ET+20b7uhL92ET+yij1xTj58OD059DzuSnKkzLez57urift2rD+xR7+wxrboSv60UH80Tr+wRXwujb678/79uTy4cjw477////6vR33zD/3txfJnlfusirz5Mvm2Kz68M/zwzr91j/9xyT6wSLipyX71UP06cnoqhzAjDr369b058TzvDL7xSf8xCH8wBv7vBf579LutzP1vivPqWj16tPtsjDx5cD0zHP66LX4viTfw5D6zDjcvonwsBj47tb47tH22ZLRmC3Djjfp1Kf336jy4sT16sbywUb33p/1683UsXTAkELv4bns37Xi1KXp3LPj1qjFlUnywlfx38L768Pzw0366L73zVj00Hr2uSDGkzzYt33jyZj10YL0ymj84o3n27Hw2anfsEvkrzret2fUokHQplvurxzyvUH756j822P82FH86KLnu13O/2obAAAAN3RSTlMA5g108AIc/gcWQpdK+y/lLNxfPrdy31TEIMHRpM/S+2LOueyzgYxXmfGDi/ws95/hoq6uZ/Vrw+95LQAAB7tJREFUWMOtmHdYE2kawIelC4iIKAhYVux4W+85264u6ynHhd3InZ4nu0E0uWBBcigJHEcgCCSUALeUgKEtoASWdjTp5aT3Lk0QBfXEvt3dvefedxKkigT298fkm/d739+8mSeZ+WYI4vUYaGivIn4V1LZrJSUlKf4aKoM1SRECN9WkbYtXLdVOEvwVEC2+s6XaGRQHEpHWIs+ZgXaEm4MM0W+WLuq0r5lQOTgUrXxjES6NDKu/kVzBzYUIjYWrViVRSNOZkZoHbvBppbVkoao3tAUXkO7rQN1JGAkW/C01qklV+/VE5IEbjBf6LZdpWbkB7YlsNjuLzU5shx2K1rIFuVaIUNWXyM7KCg4OBttd2BWtWIhqSQaq7oIq+CsAZL2OEMhYyOlfIYBKp1521k9ffQmg7AFEBKsX8OfJOAnUsbPqz395DSBldyEUIX9j7wisrKzul2TVJ//vWiiAMvY9ipWVYLYzZqC4YqXhuleo1LVARb3Hfpbs8HPoFwDIgsPZ3RDNmP63VF6y2gYxhZ/kum2Kq1SmzSuKoKqv5L8XkutDx13QWK8dNPbWVNMqQxubG0UCO8dqQmUlKTVUnKJbQ6VQjvay7yaf/IVUjTfWTqFQtNQnn1foKUNAQajE9tHqoqKIUWjxrYnf4bIimIO2kpOfZYd+8S8AXU3h/BdHKRTRxFVRXcPGJkJqolC8ifV25EBQNGpj+o6BLGebExzmHv+n5H9fCz13Dl3n7LOz44PDS/pgYs3L3+AGmyTB0fvdXFLhRGyljqN6w9lUg+xNWRt260v4x6i/ZNvbnyOxt8+Oh8Z6vanUajJJbcm7pqOqV+pqoqKidmK9iFAlPY7dT9rrnVRHnZ3Xb1+37t0iCD3lh1v9nA0uGdBXUzC/pJ5KFWwwXL3a0NTZOYP7NB2Juk/2QghIVx3Io2qe3m+94YxcpVLPlvDDn8VnZ38+yRXfxOI/hWxpUkbbk/T0i56enhfTv0ZHBOHoDdilR3E46RxO1Ei9qCgiQhVCfUxWUFO8re3nL7ElXa5nvL0dBSKR4ERfIpgimUxXz4tfo2QrYYec56AfjsDhjHQ7kaERPitIONlla2sb3+TPYrbj7LHuuutQ48rkA0zPaIytJK6SUxhmkofgcGpG6p7U3WMGoCvQdhKBQmEQi88sefGiBr4GlvACWCxWQKXrDZSsJ044Aq2ezMoAgFfpGnkRiWTyWP5CYWDgXyYRGCgMYrBkKZ6RlWBi+PszWAHMapS8R6g6AdWRlRBnwDF4lUzXyEhXyGMEzeIS+jPwgK6uTKkpKCgIItdFKNlEVOOHKJLnC4fwZzB8fXm8SsiDXQ+h8M9T+Uwo9PBn+AbweJDgy4AUAAI16HDaTGwlXek8ur+Lh4eLC41O9wXoDNh1d/9sGu7uHi7+DEih0+k0LMAA3e9bdKjuIDZcRWr86DSP0+6kjcagM2g0VJ2apjrl7n6azABcSJPU9Q0q8tWI9SeQb8HldRrx8vKCTNjC+NQMZBle0nlIOO1F8/X7DhUbCOK9fyLfNJTSvI4cOnToyASHZmXa/BEaza+hERXGBKFHur5rKC0N2/UJcOnSJUyEj09m55IU2c6usFK/9GFUbCII3VYcPB7zKw37YJf8hIWVNsSholoTLtGS84Bpem5pWNgHcvNhWE5pw/dokOBS4+2zQGtcQ1lO2O4P5WT37pyc3LFHaHgbr2lGODr7Q0puTs7e3fKyN6csNy8T6g9vQdfyfnQ1cnLLcj7aKy+gSvm+FeqLNcnr//BhwCYuJbds356P5GLPnrKylLEBrM9UI+8Axjg+nDaWUhazR072lZWnxHVBdauJ9G6il4+uxryU8piYffIRU54yducMVEs2S10qmejqf5haHhvzsVzEgCuvGat/qya7zem3HQMG8irKY/fv//282b8/NrYi9WE+1OZvGb9lbh4+A0geppKy+RMbW14R9whrM3VfLi8U2jAwEFfhkxD7u3mDqpab+VDZrz+xwNB7jq7imz9W+Bw4eHB+poMHDyT4hFxuxsrnmyc9qyj0/wNovJwKnR0A3Tw4gKq4tDaoy9dRnrTy0buNrra0jhAfnwSwzYMEUBUURmPd7eVTHqJ0JBgcvJmHsj/MA1T9WNiMVbXGylNWd5ri/v8Aj80LQiwsEv70WhIsLEJaOqvaoOa4WHPaotLo9t+RR4UFIdYWFn+cm08tSFVaPpY8N5mx2FUYwonjVVIZ8ulMLGRYW8O5SivGilollRkrZ02z2isA94fCjoJUa2tri1cAUyEteXEd5mnFZL5Yb5Zl+Eaz4uMAt8rc3LyzIy6voCU1xHoyIaktBSC53GmOVOVj9k6xifIsLuU3xT3kdGPVHXMZhZ2XgQ7cdBaOB81vpg00d1liruVtBfXZnz2NxeSxgOKuxkdVaS+VMsWdtKqB5sddkuPjWGYq7XjFw4aKjrjHcgo9kujo6K4u2AzWci1nkLlW89WvEHTEg9yd84NbO3Rr7fI5nqhUdMzMbg1LeuaUFEuGMm+JzczM5lSBzEgJkszEtzKHhwZri3u42CaXy+0prpUMDg1LHSRK+lt0X/t+Q3fjmwpmc6BkbLLpfV31eT89qusu32hkoq+joIRtrlVSUtDRNzHatPF9zR1zvFX4P2WXaMF+IzEUAAAAAElFTkSuQmCC',
  'top2': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEsAAABBCAMAAACuC6m+AAABS2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDAgNzkuMTYwNDUxLCAyMDE3LzA1LzA2LTAxOjA4OjIxICAgICAgICAiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIi8+CiA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgo8P3hwYWNrZXQgZW5kPSJyIj8+LUNEtwAAAARnQU1BAACxjwv8YQUAAAABc1JHQgCuzhzpAAADAFBMVEUAAADu3cLv3sLw3cPaypfw0LHj07Dw4Mjh0aXq1KXw4Mfby5fu4MTbzJbay5fv3cPby5jaypfw38Xw38Xu3cDay5fay5ft3MDay5fazJbv38TZy5bv3sTu3cLz7d/t3cDay5fay5ju59Dr4MPt3MHv38Tu3cLaypbZy5fs3MDq3L3s3MDazJfr3b/ay5fn2LXZy5bbzJjZy5fay5fx38bt3L/u3sLh1qzg0qbr2r7Zy5fdz5/i1q/h06ni38zP0NHu69bu7NjQ0dLNzs7O0NDIycnOz8/Mzc7MzM3LzMzKy8vJycrHx8jay5fKysrIyMjGxsbGx8fFxsbExcXR0tPFxcX7+vX49+/BwcPDxMXw7t329en18+bV19n////Z293a3d7P0dPOz9HT1Nj9/fvCwsi4t8HW2NrHxsy+vcXR09XY2ty+vb/t3L/S1NTU1dXMzc/y79/+/v39/PnKy87z8eHBwMG/v8H7+fPU1tfKys3ExMj08uT49u3u3sPFxcv5+fP49ey2tL+wrrvBwMfw7du9vMe6uMO0sr7X2dvMztDNztHAwMb8+/eurLrV1tfIyMyHhZDv7Nn9/Pq8vL64tsS8u8Wzsb3Dw8O2tcK7usPt6tXu7NrEw8v6+PG/vsirqLby5dDGxsrQ0dfCwsXJyNHFxc66ub6rqrCioanKy9KbmZ/q59Ty4cn48ub069nk3Lft6dLOz9DJyszIyM6CgYzBwMvi2LDs6M/Ly9C9vcKlpKzQzsP49/aVlJvl49Hf3M3n5dPo4sTOz9S4t7vCwLnFw76cm6Xx48zq6uzczpz19Ovg3tb37+K6uLPr6dno5trJx76urLfe3d/09PT59Orn5ujp5Mfy8uz29fDt7One0KDLzM+OjJXU0senpqjx8Orz6dXn5uPr5szf1KjKzM7h1qzx7+LW1dGwr7a3tbC0s7fs6t/b2c7Ny8HY1ci9u7bj4uHm377Lycaxr7/Z2dWtq6qQj5r17t/a2dqop7HS0dChn6L6+vrLysuxr63mkIztAAAAP3RSTlMAQiku8AIN/RYH5VkdPrO3ch93m2Di2sGiTFL7zJf9+Mbp8eRy0othvKXP/ZTu0rlIiCx/3Yfa7qfTZ87cjPqxJQDEAAAIx0lEQVRYw62YeVxS2R7Ab6WDadq+17Tve71Z33sfjF6TJU5mhuBWYqAopilKgiugiKJDuUug4r6Lmrmk5pZmammpWbY4la1a1puWWd6b3wVLLTO1vn9c7j3n9/vyO+cC9xwQ5NOMn64+D/kiTFyJycjATPsSqkkLMmJbnLIyvoBsgnpGpiPQ8PmyCeq1xxwVNGBmfOYA1WMDHftoWDThs6Z9RayT4zuy/vHVZ7im1yqrCj4fDMfDsdPHrpqHkR4G/moNDcq/AifHMOPGqvpKPVOhivsv4IvKMsc8yoWdqKokwS8yNDQyMr8ELjoXjk01HkMIDg4uSYzk+wL8yISS4OBjmLHdy8kNTk5OJamhvtH5iYn5+b6hAoKTU8PksajG1QYGBhIEkWH5iQmpqQmJ+WGh56GldizTP7kFMltDw/JbUyvj4ipTQRZ9JTCwZfYYvjxoWe2+/PzzJXGWQFxqYjTfkhAYGDv6wtZkEghecaHRreatllYCgZllZYLEN/QagdAy1IxNWj752xUf+76qYQgEwrVQ38vmjrZWtocO2VpZVubl8xPaCYTY92+l6rjZHijr4CM545tp8ya917+8gUBot+YnOJqfNwPVoUMCMxilb2QrgZA5+JukOm+BhwcmK9PduxOZtMwOZe00tYER30oJXq2RvuehLMFB1KUsLEwChWEGRk6YbWcXC/MBSJGVHp1ZWbEednaLpo/vj+j08mr35Keam7ea2SpcBwWK6Y/81cvr9fL+qVi4zi62xUuJO7LAXXHSkuVht27N26F+A/1QVqv5FUurPpetmU0AzJik3Uu64t1ncINdRmb7ZctehcIbWdpn9ZK+xtgtWqiYWFV1KKuQH33lLzMzwUEl6CALJb4cKKxTETRx3Jp1Hj690c0xMTGXjUBwH/lainLVXpL35urXMNS1K2fM2JQllV6jhkkOH7Q0E9i+dbnaBFhH8/PapZkbfpg9+4dFdna1vVFBKDGeF8DxNWKqUEWBPKaZdjkLo7gTJlKjN9Roz95K1KVEgLryKGGca1KpMqj2EiWIGw5wg8Jug0SOeBsBvTFBQVwuNyiI88Y5Kzb2tZHRVV4UJS/AxtUKPqhKrMwsLQo9o6m/uBt5mza4md4O4HLDOVSA40e7DZKliDsc3Q2DuH4cDscP3oJjj7Yb/cqhSRiV4BL0u5SFoXnu1+yFClMYLZoWxucI0ZxliAkcvVnQHBUWxaeitiAOT0ahRvGsCy1sXM2s3gEuMsOTxudwwrnNMDo/ahSNIpFIeDQq/ipY1iKm7u7uF0R+fBqPQuHRoqhoceHhHD5NkheAuvpxdbWxKLSmgMwvPNwP3pzGk1iLRNYSHnXXBbBsQoxR1/+oYTyJzNqTzYOK0SmAd7TuIBsesf9lAPY2FgEMTwoN6qdGgYktY3UUdrBkFL8/UNe/EbG3t7eJDlXIFjEYDJa1J0VIo9GEQoqM4Y+zsXcdiP0RQ6w/S8bmCQEKWyZi+JOx5A4RhfsAlWggS01MTC78ES6UsRz8/R3usUQyNiATsRzIOKhrEEcMcWQHiJCxPWUiEcMhB4vDgYvX/BtIjm5FlsGLyQMuuPyxWKzCBuQqAg2PDMbQEIfNgQhWLot1zwGKwhnichgyYXM9OAomImtR128xQhkjAGdogSUH+Dt0dHQoAw1t3gOVkf0VBECABVz7szxpD3vBsQFBvkdd9TE0NtSFw+GwWDI5JyeHTMbCleEH9EWg3Yp+HNYhl1KU9AQcmxBkiilw6WERRdRBxilC34IbmkG92ByGNY+79z44tiCIpg+8PglJggnbQcaOlhyHXLbw9wdupqbam+EnusLNze3+i2YiO3fXT/8ZLQ73ROyi5HpQVKBLje/gxO1BcRFbV2fHT6Nkx65cWXxS1SUw/Av9TfseddUnF8Xjc3V2jBKd3bps5u8v7ru5Oa9HXRpycD2hP6Sxd+65t2t06EBZRcXd4LozV/H7n+3s7Hz/ZkpRvEx3t86oYOXqsoVVjy+B4PRExRPgn3DqXJ+cRGTjc3ePij26+PiilBfGzs7GWsqnyZQCcD05V1wUb7Dz5z2jQRdPIiYV10N6hYbSNbXrKNCdksQk4ff+PAp27jUg0lPOXYTs7yb2PeZWa8PVpWfFRTDKvbojB69PYlaldBsfPVow5+0jUyMbXMbdKVXMCAM8fudIwe8jEUNqFGWd1ny3vJh5Ai2suibEJUJ/394Rgtc/QKQ/vIWWJV7cv8CYkm1sbOzTfSuZTiTp6+NHhj4pnpmU1nMRUk9rDNirLJFDy8WetComkWRgsG8kGOyPYIbUNJZBYsEs1QErnyld0GRc9qw0iUmMOKA/Ag6QQFV8q/sk5HWtGrSJmlXh4+Nz8uzLmhCobL/BJ9lPIrrQk9POXIS0to2qg1Z3m+vEJ06cuHjzzxSQRZBIB4aFRIogMunJpefKIWn79bnvLSrndEHzifSetOIQOhG1kfYPCXSQIiKIRGZIcml12UnIydb6YLG7JBuVlTeVpiTRmS5EIOJD0GaiiwuTTk9KAdV2yGhTmfrBynmuXhtacHlT2quah3Q6k8l0AYj9oJfQCp6q5JpXN9KaFCp53ZQhluFb6u5sB8rP/D/t+Knjr9JSkh+HhITQ+4DTqsfJKTWlN6DzRtqtxpvlaLT4upbqEC7V1XUFaHf62ac91Y9e/olmnTp1vI9T6PmN0rRbLx9VN515erZMjMZqd81UG3rvufF6gbaS9LKzT2+e6Wmqrn7W2PjoUWNj47Pqc009Z24+7T5bli7Wfstpla0f2WxMnflOpkQsTi8vLwPKy9PTxWLt9+maP/ejm6CpM+uey7VHhrzt+d35q4bZUU2dpad3N7uiYFjJnYrs03fr9PT05msMuz1Tm6MCQXp1d7uyn1e03SmQy8VisVwuL7jTVvE8GxzXUQmKyuL1mp/8f0Nz2+olesOgslFry4+aaiPePapprto2R2vxrCUqaJnzVVRmzlqsNWf9th83bx3mX4W/ARH+uYHyqPYqAAAAAElFTkSuQmCC',
  'top3': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEsAAABBCAMAAACuC6m+AAABS2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDAgNzkuMTYwNDUxLCAyMDE3LzA1LzA2LTAxOjA4OjIxICAgICAgICAiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIi8+CiA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgo8P3hwYWNrZXQgZW5kPSJyIj8+LUNEtwAAAARnQU1BAACxjwv8YQUAAAABc1JHQgCuzhzpAAADAFBMVEUAAAD37dvj067w4MPaypf06dLw0LHx4cff0aLq1KXx38Px3sTby5fx3b/m0anay5bay5fx3sHby5jx4MTw3sDx4MXv3b3v3Lzay5fbzJXx38Pw3sHx38Pw3b7w3L3Zy5bay5fw3LzbzJng0qLZzJfy3sTx3sHaypbay5bay5fv3b7ZypfZy5ft27jZy5fu3bzay5fy38Px58vv3b3s3bvx4cfazJbh06Xg0KDaypjczJrw3Lzay5bZy5fw38Xay5ny38bl1qv16NTl2bHj1qzgy57m1Kvh1afm06zdsnTcsHHcsXLftnrft3vcr3Dgt3zds3Xftnn47czetXjdsXPes3bhuoDftXjetHfetHbay5f9+vHXp2jbrm7ivIPhuX7ftXrYqWrbrm/WpmbguH3gtnvbr3HguHzZqmvbsHLarXD///79+e/578/9+/PjvYTZq23csnX8+OvYqWjZq2zdtHfVpWXarW3kv4f+/PbSnVzdsnb68triu4H47s7Zqmn+/fn+/frjvoX689z68dj+/PjetHjarG7ToF7Uo2Lw3Lv579LVpGP8+Oz79uXw3sDarGzClk/csXT58db69OX79Ojw3r758NP89+n79N/iu4Lv4cD89ub05s/37Nby4cf79eP8+Or16cb268n//vz37M7z5M379unToWD////16MXetHn+/fvr2LjNnFfUoWDp1KzjwJLkzZ/m2a316sjx5cDq27K4hTa7iDvp1LLez53RoFy9iz/gyJfEmlX68uT48OL27dHy5cfQnFrRq2/y4srKmVPKoWDQmlfAj0XHnlzYqm/79eHq167n0qb37Mv479y2gjPw473fuH3Vs3vhyZ326dXu3bXu4bnfxZPmy6X8+fLOqGvz6czmyJ/Sr3Tmz6nhvIzr3rXi1KXdsXTs3bzj1qjRo2LFkknBk0vcs369jUP58eDl0KPMpmf058PTp2nasHfYuoXWt4DbvorTsnfczprZvIjcv43bsHvIl1DjxJnXuYLt17jfuojZvIY7hbiCAAAASXRSTlMA/g3k8P4C/RsHly9ZLBRL4rdy0kUfzvyzQXhyUqBi+9rA0fwq3YthnML9uqWw6e5/O/KHyeg6p5HIhtOPZ1np9dzw5sX8d9hPWKdDVwAACNFJREFUWMOtmGdAE8kewJejiih2sdezne168e7da18SEohEOIUI2Syum7wAiSkoRLhTTAgklFCCYsAT6UdvUkNH4FFEQLD3rpz1qvfefwOcYAW834dldspv/zPMzs4EQd7MdEvjuchfwrhl5nV15nP+CtX0qfG6qH1G8bPfXjX+4zr9f4DKt5eN/zjp9BEDleZ/e8sOGut2HRmg8u/j32rYp+r2HfkTo3ffeQuXZdKuH0i+6yav3+osx66aG3/aoDL6xSeHlJ42nzZW1TvG+u9InmYFBgZe3AUp/Zh7adn9LcmpLKVKpczKIdOmY+zlBHNPg0opZQGY8gLcnDafMCbX5Mp9QLGKRSh4PAXBktbBbeXksajer9sFJKlYAp5GKNTgAlZzFGTUjWX4J5Mto5pZAtyXTaezfUGWQ+Z8NIaXJwkael7ECK45293NzR1khDQe8nTvj9o1T+/p6RkqRfG6KLcDrq4H3Ni+PPQ+39Mz6mUjNn3O5Hc3zH+FaqI5qNQ8Fi8+NdTVa88erwMn0jUCrBhyk55/Lc2mLYomWQVTcv6Hs+daPFc+5w60uoAR2alHclz3AK5ONAisOdLTUz9vuGnujOjo+Dj9jmRTxMLYIJ0xe5huKt/FJaoZ/eOH1CRQ7dwJgbmzNQrWORcXF/OJQ8cVYkrSu5DwkWWhpkZxutDo6Jnzns3DCd1QdoElME3dl+MFqp07vVyd6L44ika6uLQ/W68nWq6K1kW59KNGZsQYEpFxodGr5k0fqPMhlCc3oxdTU5+So2Xo5O4T6VwBdorvwp86qJq2Ljper657ampQJCP/5A/SHh8009IQm5kx3GZgglWpRgcGXF5e8K8UwohF8fndhkrjpq2ZGWrUnRN47NgxU7J9JWJk8Ox4eu5UaHJ7aFDQjGXz56+J4/NjbrF4lad3Drr2QCft5VwCK+bz9es2LFq0YWZQULXpHz5ATU2GGhxGSKSa5CLIjwVerIuLDyIJUau7MRTfdc5tt6tXP66uuw2jfytGre6vVN39m49PYFZWVqDPqShwmCLJpEpfU+NjeMIv5nfidLp2tXrHOZZCU3zCyXXQ5QXzlS6Bya9Tq5P1d+7oI4ulPilKKaBK6aoEyQfI9zuAOp/AFKVSmRLoU3NrZhSZE3kf5QnZ7k4w5wfZ7URPh8B+S4bi9qc5StLEQgFM1XUH8oyRyO+BjEAVuUphUlKXcivnXM59loArobs57R7qcqfBW4mxeM1KH59S0kQuRzwFil3Qg2QGcigmJuZgsRJDBQqFAGVJVeSKnKViobwr6fbgGoKTG83RFlqqslJSsuDhqIDH1WhsOfBu6cGyBjGCa0gxuUxxuThHQEBwKik80ZvLoJ1wcxqGG50pxBVkFSnGIgQcrlAikQi5CiwjBCyfIaYHDx48lIERPI2vxFfD5SkEBIoSYPZNp7k/77KnyYVcnkBAEAIFxCRJZ9PYEo3ix6sgCbFCPiD/ZEgFOEXOZMolFA2O83g4rmHIadtfcLmDjKHh4jjO1Qglchrd3p62GVyhIGlfihiHhIQcAheXwaTRaUyIWQMIGRImnQ6L4DDcTxhq+AqFQl+JBOrTIYPBFdSag+TsOOSjQ8DVHwnclyyjsZlyuUTiKE8n77a7P4f9QA05lLOhAoSVTsHRzmpwrEOQT/8LxNYSOIVJ376drLxx4zcbN9Ig/u0vgaxAMxT3l9PkV3joI1Nw/BtBppCu6k6YAnKaPcm2bTRg2zb7V7GNZDDNhlki7YoDxxcI8knc/v37dT+zFDBg9FFDY1Jw8fVs2f79VbNgiY4FV1zXdTFO2fwNbbQwJRoeeuYqKGLJrca/ICHLvo5ybLcwN44WJoMahj2sBsU/yDXt073A1VpRmB0ENko2O27iEKXlVXv3yj4jXVZnwVX9E3RSw2COEgmD6402ZAeAwJp0WVwCV1VuA+pNpThuHhVbGLYcsfRxrGzv3rxxhi/AYplMFlB4BhPjtowto8KRQvVGM3vPymQRNv1fkynVIKv+qQbdSr3iOCoomzhh2L1srUwWa9XvssgDV9WFBjIwIWMUUGxxCOtBATRfMm7gM7eyKiAgILozUxSGUzdRRo6tHYfAmu5C67MrBj+ZVoXgSsxtkhIcqu3IVZuonDBR/cPoiICAvE/+3F5MqoqIiOjprBeFcew2jRiqnQJNabibGBGR+PmzDcaUQnBBYKUiWHeptiODi3uLsfAHsdqIiDyrIWeVrxO02uDYB+GY2BvnjkxFxbeKRb+eyU7UavMXmg3Z+Uy5ptVqE1tqb7DE3hycOgLsOKDKrOithobXlg87RC3sgLy0rqZfRSCzG5EqzL/05u89flptwWKzYbs76ydtwcEesY/CYWKEbeXYvQEwgarxfEticLBfifVzm8rV14KDgxNPdt7MFBEg47xSB0WcrQbV7TOt+dCo0OaFze7aSx4eHgkt5xszWf5isTew9UXIbO8wsdjfn1V/+0xuGjQpMLF4Yeds7VwAJYdbaxsb65UilCDEYnHYcCBHTBD+Iqy0/kYZqPrIp5dMeck2/Isn8Bi/tNbz4UeLispu/JpZqhKJRP4DQBJTBWbW32gsKzpeVNZY8bi1z8/Do22JjdlLXGbvleT7+fmltfx8vqHiZmPZ0aLjx48XHT16tKwMLkX9d2W3b4ZXNN07/3t54WGo7Hdt0sSXnz0XG2QJBSezc3v/11l7r6GpoiJ8kIqKpoZ7tY8fPurNbW052dHXRqryTJa+4rBhManE8DA/h4S+jpOFLdmtd3PLy8t7e3vLy7ty77ZmtxSe7CnoS3Dw68chb4H1Kw9BSyeV9CQ4DKEtPy2tD0hLO5zf5jCMhIJLlxcsf82JymKhs/Plwo58h9cAMV/Ku1zi7Oy8wOq1xzOL1SZQybnkcl5hT0dBX34CGWZbQsLhvoKOnkuFeZeXPHHux2Tlillv/H1j1vr31jq/BpPFNl9+NctixKfHibOWr19hs3LhWhMyzAUmJpMWfm6z+sv1X1kvfc2vCv8HNvDZfRKnAxAAAAAASUVORK5CYII=',
  'rank': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAABHCAMAAABmm3oQAAABS2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDAgNzkuMTYwNDUxLCAyMDE3LzA1LzA2LTAxOjA4OjIxICAgICAgICAiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIi8+CiA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgo8P3hwYWNrZXQgZW5kPSJyIj8+LUNEtwAAAARnQU1BAACxjwv8YQUAAAABc1JHQgCuzhzpAAADAFBMVEUAAADx6tPo2rLRtGHdl3TRs2b8+/jRsmLOtmX58+jRtGL59+738uXRs2HVum/7+vX7+PP8+vfRsmHStGL17trRtGL9/fzk1KbStGTRs2P9/PnQs2LRs2LRsmLRsmHZwX/i0J7StWbv48fXvXfbxIT69+75+O/s3r3PsWLRs2HRsmH17t3u48T7+vXRtGTl06Pm1qr38+X28eP59+/dx4vPsWPs4MDdx43Ut2v69ezc0ZjQtGLq3LX17+DRtGLRs2Ht4b/j0aPbwoHi0J7079zfypTq27bt4cL179/x58/17tz07Njv58vgzJXRs2LYwX7Itlvm1qvy69f59uzPs2PUsmbv5svRsmHfy5XQs2HXsGLy6NHx5s7gzJbgzZn69+7279/r4cTx58/w6NHm2K3SsmLdx4zbwoTi0J/hzZf////lwo3gt3vftnngtnrhuoDdsnTguHzes3biu4Hiu4LdsXPcr3HhuX7etHfhuX/brnDjvofarG3ivIPjvYXjvobbrW/kwIrjv4jaq2zYqWnZqmvXp2fkwYvkv4nYqGjXpmbWpGPlwYzWpWXlw4/Vo2LVomHlw47UoWDmxJDUoF/Tn17mxZHmxJHTn13mxZLTnlzSnVvnxpPEkUbDkETFk0jRnVn///7Klk/Gk0nNmFLMmlPHlkzPmlXLmVHSoFzeuH/lz6zXrGrLn13//v3Qm1f48unkwYzivYbct33x5dPbsnXPp2vdwJPIm1bImFDv4s79+vf17N7auon8+PPgu4HWqGbQoFviyqXiv4ngvIXy59b+/PrfuoDSnlzZrGzixZnq1rnVr3bz6dnImVL69u/UqGjOnljTpGDSoV3WtYHly6LGl07o07Lhxp7duoPIlEvWrGzft3vr28DZsHLQol7SpmPet3vMnVfatHrdtXnu38nVp2TVpWPhw5Tgv4zPpWTOo2LRqm3NpWb27+Tlzqnt3cTZt4Lp1bXcsnXZuIX27uLmxI/ewpfVqmrarW7ZqmrfvorbvY7ctHbVsXvWrXDnj0rjAAAAanRSTlMA0LY0Ahn3jAXlVOzgJZL18PY8LtZI/K4+E/lLRXJfTqmOxpWb6Oy+KnZY2cPzaWVyyL3tnzu/oJDdBza53G19azg4XLFXgIiqcKOQQKVcmQ6x0+ggHspipIUNeklCh+GzGmFZs1CBkKumk8PRfQAABcRJREFUWMOlmHdcU1cUx19I5IalyEYQUdkI1lXjHlXr3rN7t5+orbut2IFVaW2NWqrVyssLgYQQ9gYF2UsBFVTcIgo4i1q1aJfvnfciRkjyLv3+k/dJzvnl3nvuO/ecSxDGGDlz2oypgwcPGjx1xnvvEl1h5PvTRzhJ24n0nj4TV+ONd3pKO/L262L+EqJpI6QGsBrPV2T8AqkRBk3gozFhkNQ4PaeYFnnVXGed3FR/6vqNkrPa8pK/rp+6X3ro2TpP6mZco9ukSE6i/oY2tThFFr43IiJif/julOIS7ZG7OiHricZE3rRmrXKPpBXL9kfoszel5GzcMdZgyGTDIq/YgsnhU2nF4fs7Q5aadrQMbIIGGhIZGAQG6Wklsr2GWJ5/9iFYmb9kYCSsyNGrIbJfDSNLvXoR7Hp1KtMHphPZqg17FG6MR2Ha1kQYTSeTmmgB/9CqTZGZIkV7BGRs+3QIsYAVSQvbbZowTsba/sXNBiIXL4Us50No2hmw7zdXT2Qy7Njc//JTeKks35xxn3Fw0lvhhQ7Md4euakN/4sm68+mMi7fLcypTYHxFGZvD+BKqrTrM+Pi2J5xRkAoeHs8P5U9Ixp/wgj+Lk8gZhpKXEfI9Bvlt1bDAr3EqQtgqucfLN+OQeu0e7D3dYN6CoWRlpIZgkd92kPEzEz+34Y41XPkGj9SMB4yjhRBULHvAS9hW/jUmVxqSGc+PYG09YUL3Lq37CpPy860QbFdaxWYA7LjLf6zDZe2lKsZ1DLPz3MdAarp8bS02/xQyCdQpmFZZAodgfcOVz7C51nCX8V1KR+ljNs5tF77EZuVxeLVHLyJcPwGVqvjVn2NzoQ023uJRhI0ZqNw8v2I1NhfibzO+Dm6E0BdUEipXdIH4WxCkAMIFAi2tqfy0C1Q2QuJcQrizKjnxK7tAfA7juyCYcGdnlF2w6gts1hSoQKU3Yceu7q2G9WuwWdWcAO9jb0LIpqiqyxtXYbOh9iasy1jCbzio/H2icj02jwv/ZXwDg4luEjig4lQFG7B50gi5V+BBiJEP81Stbni8EZcCTT3jO8CSIPznM0+JNYVbv8VkU606F3KmO0EEsEHKUzVvwmRLIYRIupTOmaO84LFJXffkOzyaNXkwje4LCcIVeTPPZY2NzVux2FKnboJCGo2k01Tvfuz5qq7btgWHFlUChFfgz6RvR3bHHM5R1f6AwbZYdRE4OlvCeYTYMqoo6kTLNt5sr1Vlw3nvJPGDA2ksG6WyBHXsju182dlIwTkiDUTs4SiU9GDDRGrqWnbwo6VQncBWvp4e7DktRi+zFXUWpYr9kRc7YzUklEFSH2TDHfeOErbULcukZXbyYFeFhspi/9nMX1fAiNForoeooVT7dpmmQkXdZufj09fuWTHlzoVJWn2aUlX8bAp6JNlsWxHpiUTtLR4azjWaBylKo9jzizH2KDRUTS5rLkB2z5WHQuTLtT4H1ZRSUbHHMBVJSrKmmusiJZZ6Be+HKJCTKc0hlTGKfYZQqJRkwu9c7+eF9Ls2kUd3K12nlymPUiYd+K0zFDHKKDlbo9JdySw07IUS3gbNseVkyrLk5DlNTJLigD6KJBWtQcVxTaH5LOTYoSkZhuYM0fWX6XdoHWU0LaQAKfojKSYmWhlFyk9ys5HaDkVunTRILqi7tU4m8WKmnKTO0UrR0bR7dLRSeY6WIE+W6iys+3cqQhB2qK+gvW1Ozzstl5MkBZCkXC6/U5Ss+9HJFyEXA42jPUIfmLfrJJa2PsjMpuTy09mZJ880HWv/xdoLIT/D7bQl6j8/0URvL/VxRsjN6LWFI0JDHYzqWJn1RcH2Jm4IFgXQOoJeBiR6OXgipLfrDWEzGyGJs2BIB4kgh3kShPyH8byDoZeHpv+8foEWtsyozIOsxiw282K+nG2DcRUk9rP0Ry8yLsBejH0xNdfe0WPsuGW0+7Jx/kvchK7E/0AkEpm0eQpmDRCkaVN8XgAAAABJRU5ErkJggg==',
  'self': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAABHCAMAAABmm3oQAAABS2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDAgNzkuMTYwNDUxLCAyMDE3LzA1LzA2LTAxOjA4OjIxICAgICAgICAiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIi8+CiA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgo8P3hwYWNrZXQgZW5kPSJyIj8+LUNEtwAAAARnQU1BAACxjwv8YQUAAAABc1JHQgCuzhzpAAADAFBMVEUAAAD59+3RtGLQsmTRtGPo2rLRs2HRsmLVvXH9/PnQsmL9/fz58+jUuGzy69S2tm3Ss2PRs2H7+PP5+O/TtGfStGT/iYn8+/j38uXdx4zi0J759OrRtGLk1Kbv5MjRs2LRs2LRsWH7+vXStWbbxYbWvHX69+3Rs2H17tzs3r77+vX07dnRs2HRsmHgzJXu48TRs2Law4Piz57l06Pi0J328ePw5cr179759+/NtF3Zwn/x6NDPsWL28eL179/RtGXv5Mjk0qL079zm2K738+Xw5MfSs2Lv58vm1qvr3rzPs2Pq3LXUsmbfyI3k0qP38eTt4sXcxYzx5s7s377Vu3T279/s4MD069f17dvr4cTw6NH07djm2K3dx4zXvnrbwoT////eOi/oV0vjRzvnUETnT0LnUkXmTkHlSz7kSTzoVUjoVknlSj3oWUznU0fmTD/jRjrpW0/jRTniRDjpWk3pXVDpXlHiQzfhQjbqX1LhQTXqYFPqYVXgQDTqY1fqYlbfPTLgPjPsa17eOzDfPTHrZFjrZVnrZlrrZ1vdOS7raFzdOC3saV3sal3dNyzcNizcNSrbNCrGJx/EJh7hWE3CJR3oZFjAJBz+/fzOLCPZNivQLyXTLiXYMynNLyXWNyzHKyH///7mYFTcRDjXMSflUUTy0c////7fQDTqaFzhXFH34uHlnZnMKyLpqKTjgHnUMCb99vXZMynJKSDwy8nGOjPpYFXmWEzcTkTlioTZPzTJNSzRLSTJLiTNU0378fHaMynaQjfWPDLvxcPgTULcgn3LPTXXRDrtwL3QMijgUEXSNSvibGLhYlfno5/67OvCKyTOUEn01tXmlY/ORj/cVUv45+bDMCjUbWjENS7lWk/12djrtLHZamPgjon++vrUW1PFMyvSQDbJQzzZeXPbOi/glZHbPjPcPDHjeG/kXlPyzMnnmJPSYlztuLXbXFPLTUfXdG7QOTDhRTncST3acWvimJPjU0jdfnj23tzeQjbXRz3YOzD67+7eZ17OQzsQz22ZAAAAYHRSTlMA61MZNbd1jAb5O/vlkNEDLiXw7BM+AvbgoKngSK7HTEVh9Y6dlOaA2L7z1V1YpMNsTjtlWr1wqe0Nmc0q3ttoe4axccllMkCxvSC5HjhyxIVZSYpPs7+NoBpZkrOBlpBH66yuAAAFwElEQVRYw6WYd1xTVxTHHyHkMgx7yyxLWeKeRWvd2r131NZqW9taq1XbopW2KkqpeRkkmBAI8BEERWSrKHu5Ny7cKO7RWldr7nkvDEnCu/T7D4/knF/uvee+c8+5FGUKy4lDPhg6ePArg4e+M/49qidYvj+0t42onZTgdyeSarz+lpuoK8FjLLlLuA7pLTKC0xCuIuODRSYYN4CLxoBxItO4jele5NW29ThSl11yZdfu3ZKs3buulOy5urltnceam9YwH5vCWG7I3pXVKpNlZi7HxGfKZK1Zm27ohZ5/yeRs2FU9sEmSmhm/vDPxK1qzru1lDIYNMi4yyBFMdpRIZHHLDZGZmrWxGWxCBxoTGRgKBldvtsbFG2HNiqx/HzBr/KKRkTAiGw+nxq0xzsrUwyfBzsqgTH+YTspBiWxlnClWyiQHE2E0BibF7wW/gEW6QybZBIvj2L9LiG1B5JokZkX36GRgNO72z6i8ASIniwt/50KMpATsfYZ3EnkNduyJ4uMxv3GisHgjdrB5uaPIlDD82eabksIYbiw9/igfu5i5dFB5AcZ3urhwKVcKJYefYJ++du3xgVTw4NHxn7izuKEawt0WJ1dvGMpfDQt/JKCoNgN79dMv8ATYKgdqixaTsLDhFAzmzU6r8k/xgoVEFNVWYj8LJhWbj8T/PDl3fwEZ86pgML34oMJ7Dl7C2qJ5hNw/twF7foRF7DxhQqfuzf2ekBn3/oZgO+tUHPrBjqupmkvKjKozkD7xzvOLxo/5dxpmEFN1BydQm9E6FX94hfZUpH9JTHrFDewbqYvSJ0ycy2d9QUx66WnsO3Ua5cws7hntrK+JSS+HWPvwKQcLUDmkTZ9FTLp2P/YN86X4fUGlRvtpD9D+h32jIygXCLRou/azHqDdConTn/JjVco/7wHl26GqGU35TQaVraWzvyFmdimMJVhAWbOrWzFzNjFzSmvgfRRQfCZFnUm++xUxMysOwbpEUaM8QOVC48M5xNxtvA4xGk2ZC+GAyla3zCTmYT3kXlt/yhI54acMzeO735LSsh1OpX48ihLA2Zq4tXHRd4TMf6w5ATnTj6IimCDdVrfMJ+RhI4RIFKnLmfxAeKzTJC/5gYwW9QUoVoRTKMoZmeHn5oL6y4uIWJ+sqYNCGuFTQOADg7mkSV6/hITL9TUQXlsBTt9ezI7ZcVZ9cf3P3NENBZK3yJsH5xFiyqhL4vrYXziz7KK6AMpfG+EoOJCimCg1n9ckxy7jysV6cTa49UHM4cgXwrkmqpSqlbG/ciM2WXOeqXw9w9l+Cr3NVNTVtFo5PZYL05VqaT7T6iAH9rj3EjKlbvN+cZJyOgdWKdV0NfPLFoK25g5NZXuIJk4yq5RJdBkzH6cA67Ziyo8NkyjjqE5m9SrTrFYmiQuYtiLFE7WXZK7Ig200K/eJk1QJq02RoFLTTScYc1tk3aE8nIAms61P5VFaoUr4wzgJaQq66QBjbCbkdSp4eSialcloonNy8xKMoVqXQ59/wPZ+gahz12bnH2Sm7/TKpGJF2to/DbA2L1chll7fwRhaRXaaD8YBjXBkZZqr5fSWpLS8Z4V2qrDGvj1sU+gWiby6NCXWaMQwfX+Zf0uno1iXm6bK27lWx848VVpablKOWCo/toG1cfRAvgYaJBcU5K6XSTxZJpeKt+Qotq0DtilytohpqfRYht7CPcigCJYJsG1vm/MvnJVLaZoW69D9kcrlty4d0X9p0xchFyONoz1CH3a4XkjMyL5dVrBPLj9aUHbsdN3e9m/cAxGyN95O81BQWEo3vb3IyRshXztTHbUXQh59TOqYWQSgKPtubgimRSAUONLNiIRVmCdCXXaJIRzCEQrwth3WRSI0bJIQIYG1HberE93y6Ph4kk+fXo5WeH+FmkWPtBiBPwx3ILlOGsUToGcJibC3JL6YGm7vFR4VEoLdQwT+vnxn6n/g6urarc1T9HOOxJkOkOAAAAAASUVORK5CYII='
};
__this.imgs = {};
__this.indexPage = 0;
wx.getSystemInfo({
  success: (res) => {
      __this.platform = res.system;
      __this.pr = res.pixelRatio * res.screenWidth * 1.0 / 375.0;
      // __this.prh = res.screenHeight * 1.0 / 667.0; //不进行高度适配
      __this.prh = 1;
      // __this.pr = res.system.indexOf('iOS') !== -1 ? res.pixelRatio * res.screenWidth * 1.0 / 375.0 : res.screenWidth * 1.0 /375.0;
      // __this.pr = res.system.indexOf('iOS') !== -1 ? res.screenWidth * 2.0 / 375.0 :res.screenWidth * 1.0/ 375.0;
      // __this.pr = res.screenWidth * 2.0 / 375.0;
  }
});

__this.sysInfo = wx.getSystemInfoSync();
__this.platform = __this.sysInfo.system;
__this.pr = __this.sysInfo.pixelRatio * __this.sysInfo.screenWidth * 1.0 / 375.0;

wx.getUserInfo({
    openIdList: ['selfOpenId'],
    success: (res) => {
        console.log('success', res.data)
        __this.nickname = res.data[0].nickName;
        __this.userAvatar = res.data[0].avatarUrl;
    	__this.imgs[__this.nickname] = new Image();
    	__this.imgs[__this.nickname].src = __this.userAvatar;

        wx.getUserCloudStorage({
            keyList: ['score'],
            success: (res) => {
                console.log(res);
                __this.hisScore = (res && res.KVDataList && res.KVDataList.length > 0) ? res.KVDataList[0].value: 0;
                __this.selfIndex = 1;
                __this.__getFriendsData();
            }
        });
    },
    fail: (res) => {
        reject(res)
    }
});
wx.onMessage(data => {
    switch (data.command) {
        case 'Over':
            __this.indexPage = 0;
            __this.selfLock = false;
            __this.dataArr = [];
            console.log(data.data);
            console.log(__this.hisScore);
            if (parseInt(data.data) <= parseInt(__this.hisScore)) {//排位未变化
                if (__this.selfIndex === 1) {
                    for (let i = 0; i < 3 && i < __this.cloudDataArr.length; i++) {
                        __this.dataArr.push(__this.cloudDataArr[i]);
                    }
                } else {
                    for (let i = __this.selfIndex - 2; i < __this.selfIndex + 1 && i < __this.cloudDataArr.length; i++) {
                        __this.dataArr.push(__this.cloudDataArr[i]);
                    }
                }
            } else {
                for (let i = 0; i < __this.cloudDataArr.length; i++) {
                    if (parseInt(__this.cloudDataArr[i].KVDataList[0].value) > parseInt(data.data)) { //比自己分高的第一位
                        __this.dataArr.length = 0;
                        __this.dataArr.push(__this.cloudDataArr[i]);
                    } else {
                        if (__this.selfLock === false) {
                        	__this.selfIndex = i + 1;
                            __this.dataArr.push({
                                openid: __this.nickname,
                                nickname: __this.nickname,
                                avatarUrl: __this.userAvatar,
                                KVDataList:[{key: 'score', value: data.data}]
                            });
                            __this.selfLock = true;
                        }
                        if (__this.cloudDataArr[i].avatarUrl === __this.userAvatar) { //是否是本人
                        	continue;
                        } else {
                            __this.dataArr.push(__this.cloudDataArr[i]);
                            if (__this.dataArr.length < 3) {  //是否满三员
                                continue;
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
            __this._Over();
            break;
        case 'Rank':
            __this.indexPage = 0;
            __this.dataArr = [];
            __this.dataArr.length = 0;
            for (let i = 0; i < 5 && i < __this.cloudDataArr.length; i++) {
                __this.dataArr.push(__this.cloudDataArr[i]);
                __this.dataArrIndex = i + 1;
            }
            __this._Rank();
            break;
        case 'Next':
            if (__this.dataArrIndex >= __this.cloudDataArr.length) {
                __this._Rank();
                __this.__getFriendsData();
            } else {
                __this.dataArr = [];
                __this.dataArr.length = 0;
                for (let i = __this.dataArrIndex; i < (__this.indexPage + 2) * 5 && i < __this.cloudDataArr.length; i++) {
                    __this.dataArr.push(__this.cloudDataArr[i]);
                    __this.dataArrIndex = i + 1;
                }
                __this.indexPage += 1;
                __this._Rank()
                __this.__getFriendsData();
            }
            break;
        case 'Prev':
            if (__this.indexPage > 0) {
                __this.dataArr = [];
                __this.dataArr.length = 0;
                console.log(__this.dataArrIndex);
                for (let i = 5 * (__this.indexPage - 1), j = 0; i < __this.cloudDataArr.length && j < 5; i++, j++) {
                    __this.dataArr.push(__this.cloudDataArr[i]);
                    __this.dataArrIndex = i + 1;
                }
                __this.indexPage -= 1;
                __this._Rank();
                __this.__getFriendsData();
            } else {
                __this._Rank();
                __this.__getFriendsData();
            }
            break;
        case 'userSet':
            __this.indexPage = 0;
            if (parseInt(data.data) > parseInt(__this.hisScore)) {
                __this.hisScore = parseInt(data.data);
                wx.setUserCloudStorage({
                    KVDataList: [{"key": "score","value": data.data}],
                    success: () => {
                      console.log('set ok');
                      __this.__getFriendsData();
                    }
                })
            }
            break;
    }
});
__this.imgs['top1'] = new Image();
__this.imgs['top1'].src = images['top1'];
__this.imgs['top2'] = new Image();
__this.imgs['top2'].src = images['top2'];
__this.imgs['top3'] = new Image();
__this.imgs['top3'].src = images['top3'];
__this.imgs['rank'] = new Image();
__this.imgs['rank'].src = images['rank'];
__this.imgs['self'] = new Image();
__this.imgs['self'].src = images['self'];

__this.__getFriendsData = () => {
      wx.getFriendCloudStorage({
          keyList: ['score'],
          success: (resAll) => {
              __this.cloudDataArr = resAll.data;
              __this.selfCheck = false;
              __this.cloudDataArr.forEach((item) => {
                  if (!__this.imgs[item.openid]) {
                      __this.imgs[item.openid] = new Image();
                      __this.imgs[item.openid].src = item.avatarUrl;
                  }
                  if (item.avatarUrl === __this.userAvatar) {
                      __this.selfCheck = true;
                  }
              });
              __this.cloudDataArr.sort((a, b) => {
                  return parseInt(b.KVDataList[0].value) - parseInt(a.KVDataList[0].value);
              });
              for (let i = 0; i < __this.cloudDataArr.length; i++) {
                  if (__this.userAvatar === __this.cloudDataArr[i].avatarUrl) {
                      __this.selfIndex = i + 1;
                      break;
                  }
              }
              if (__this.cloudDataArr.length === 0 || !__this.selfCheck) {
                  __this.cloudDataArr.push({
                      openid: __this.nickname,
                      nickname: __this.nickname,
                      avatarUrl: __this.userAvatar,
                      KVDataList:[{key: 'score', value: '0'}]
                  })
                  __this.selfIndex = __this.cloudDataArr.length;
              }
          }
      });
  };

  __this._Over = () => {
      __this.timer1 = new Date().getTime();
      let sharedCanvas = wx.getSharedCanvas()
      let context = sharedCanvas.getContext('2d')
      __this.dataArr.forEach((item, index) => {
          if (item.avatarUrl === __this.userAvatar) {
              if (__this.selfIndex === 1) {
                  __this.drawOnePerson(context, index, item, __this.selfIndex, true, __this.indexPage);
              } else {
                  __this.drawOnePerson(context, index, item, __this.selfIndex - 1, true, __this.indexPage);
              }
          } else {
              if (__this.selfIndex === 1) {
                  __this.drawOnePerson(context, index, item, __this.selfIndex, false, 0);
              } else {
                  __this.drawOnePerson(context, index, item, __this.selfIndex - 1, false, 0);
              }
          }
      });
      __this.timer2 = new Date().getTime();
      console.log('spend time:' + (__this.timer2 - __this.timer1) + 'ms');
  };

  __this._Rank = () => {
      __this.timer1 = new Date().getTime();
      let sharedCanvas = wx.getSharedCanvas()
      let context = sharedCanvas.getContext('2d')
      context.clearRect(0,0,375*__this.pr,667*__this.pr);
      __this.dataArr.forEach((item, index) => {
          if (item.avatarUrl === __this.userAvatar) {
              __this.drawOneLine(context, index, item, true);
          } else {
              __this.drawOneLine(context, index, item, false);
          }
      });
      context.font = 16 * __this.pr + "px PingFangSC-Regular";
      context.textAlign = "center";
      context.fillStyle = "#E64F42";
      context.fillText(`我的排名: ${__this.selfIndex}`, 187.5 * __this.pr, 60 * 5 * __this.pr * __this.prh + 210 * __this.pr * __this.prh);

      __this.timer2 = new Date().getTime();
      console.log('spend time: ' + (__this.timer2 - __this.timer1) + 'ms');

  };
  __this.drawOnePerson = (context, index, item, rankIndex, self) => {
      context.save();
      __this.indexPage = 0;
      let pos = {};
      pos.x = [102.5 * __this.pr, 187.5 * __this.pr, 272.5 * __this.pr];
      pos.y = [160, 176 * __this.pr * __this.prh, 218 * __this.pr * __this.prh, 285 * __this.pr * __this.prh, 318 * __this.pr * __this.prh];

      //名次背景
      let imgName = 'top' + (rankIndex + index);
      if(__this.indexPage === 0 && (rankIndex + index) <= 3 && __this.imgs[imgName].complete && false) {
          context.drawImage(__this.imgs[imgName], 0, 0, 75, 65, pos.x[index] - 16.5 * __this.pr, pos.y[1] - 3 * __this.pr, 37.5 * __this.pr, 32.5 * __this.pr);
      } else if (self && __this.imgs['self'].complete && false){
          imgName = 'self';
          context.drawImage(__this.imgs[imgName], 0, 0, 69, 71, pos.x[index] - 16.5 * __this.pr, pos.y[1], 34.5 * __this.pr, 35.5 * __this.pr);
      } else if (__this.imgs['rank'].complete && false) {
          context.drawImage(__this.imgs['rank'], 0, 0, 69, 71, pos.x[index] - 16.5 * __this.pr, pos.y[1], 34.5 * __this.pr, 35.5 * __this.pr);
      } else {
        context.fillStyle = "#FFF";
        context.beginPath();
        context.arc(pos.x[index], pos.y[1] + 16.5 * __this.pr, 16.5 * __this.pr, 0, Math.PI * 2, true);
        context.closePath();
        context.fill();
        context.fillStyle = "#E0B87D";
        if(self) {
            context.fillStyle = "#E64D40";
        }
        context.beginPath();
        context.arc(pos.x[index], pos.y[1] + 16.5 * __this.pr, 13.5 * __this.pr, 0, Math.PI * 2, true);
        context.closePath();
        context.fill();
    }

      //名次文字
      context.font = "Bold " + 16 * __this.pr + "px Arial";
      context.textAlign = "center";
      context.fillStyle = "#FFF";
      context.fillText(rankIndex + index, pos.x[index] + 1.5 * __this.pr, pos.y[1] + 22.5 * __this.pr);

      //头像
      // let img = new Image();
      // img.crossOrigin = 'anonymous';
      context.fillStyle = 'rgba(255,255,255,127)';
      context.fillRect(pos.x[index] - 22.5 * __this.pr + 1 * __this.pr, pos.y[2] + 1 * __this.pr, 45 * __this.pr - 1 * __this.pr, 45 * __this.pr - 1 * __this.pr);
      // img.src = item.avatarUrl;
      __this.imgs[item.openid] = __this.imgs[item.openid] ? __this.imgs[item.openid] : __this.imgs[item.nickname];
      if (__this.imgs[item.openid].complete) {
          context.drawImage(__this.imgs[item.openid], 0, 0, 132, 132, pos.x[index] - 22.5 * __this.pr, pos.y[2], 45 * __this.pr, 45 * __this.pr);
      }
      //名字 + 成绩
      context.font = 16 * __this.pr + "px PingFangSC-Regular";
      context.textAlign = "center";
      context.fillStyle = "#A46F31";
      if(self) {
          context.fillStyle = "#E64F42";
      }
      if(__this.getStringLengthForChinese(item.nickname) > 4) {
      	for(let i = 9;i > 3; i--) {
      		if (__this.getStringLengthForChinese(item.nickname.substr(0, i)) < 5) {
      			item.nickname = item.nickname.substr(0, i) + '...';
      			break;
      		}
      	}
      }
      context.fillText(item.nickname, pos.x[index], pos.y[3]);
      context.font = 15 * __this.pr + "px PingFangSC-Regular";
      context.fillText(item.KVDataList[0].value + 'm', pos.x[index], pos.y[4]);

      context.scale(1, __this.prh);
      context.restore();
  };

  __this.drawOneLine = (context, index, item, highlight) => {
      context.save();

      let pos = {};
      pos.x = 74 * __this.pr;
      pos.y = 60 * index * __this.pr * __this.prh + 170 * __this.pr * __this.prh;
      pos.yCenter = __this.pr * (60 * index + 60) * __this.prh;

      //名次背景
      let imgName = 'top' + (index + 1);
      if(__this.indexPage === 0 && index < 3 && __this.imgs[imgName].complete) {
          context.drawImage(__this.imgs[imgName], 0, 0, 75, 65, pos.x - 1.5 * __this.pr, pos.y + 21 * __this.pr, 37.5 * __this.pr, 32.5 * __this.pr);
      } else if (highlight && __this.imgs['self'].complete){
          imgName = 'self';
          context.drawImage(__this.imgs[imgName], 0, 0, 69, 71, pos.x, pos.y + 23.5 * __this.pr, 34.5 * __this.pr, 35.5 * __this.pr);
      } else if (__this.imgs['rank'].complete) {
          context.drawImage(__this.imgs['rank'], 0, 0, 69, 71, pos.x, pos.y + 23.5 * __this.pr, 34.5 * __this.pr, 35.5 * __this.pr);
      } else {
      	context.fillStyle = "#FFF";
        context.beginPath();
        context.arc(pos.x + 16.5 * __this.pr, pos.y + 40 * __this.pr, 16.5 * __this.pr, 0, Math.PI * 2, true);
        context.closePath();
        context.fill();
        context.fillStyle = "#E0B87D";
        if(highlight) {
            context.fillStyle = "#E64D40";
        }
        context.beginPath();
        context.arc(pos.x + 16.5 * __this.pr, pos.y + 40 * __this.pr, 13.5 * __this.pr, 0, Math.PI * 2, true);
        context.closePath();
        context.fill();
      }
      //名次文字
      context.font = "Bold " + 16 * __this.pr + "px Arial";
      context.textAlign = "center";
      context.fillStyle = "#FFF";
      context.fillText(__this.indexPage * 5 + index + 1, pos.x + 17.5 * __this.pr, pos.y + 46 * __this.pr);

      //头像
      context.fillStyle = 'rgba(255,255,255,127)';
      context.fillRect(pos.x + 56 * __this.pr + 1 * __this.pr, pos.y + 20 * __this.pr + 1 * __this.pr, 45 * __this.pr - 1 * __this.pr, 45 * __this.pr - 1 * __this.pr);
      __this.imgs[item.openid] = __this.imgs[item.openid] ? __this.imgs[item.openid] : __this.imgs[item.nickname];
      if (__this.imgs[item.openid].complete) {
          context.drawImage(__this.imgs[item.openid], 0, 0, 132, 132, pos.x + 56 * __this.pr, pos.y + 20 * __this.pr, 45 * __this.pr, 45 * __this.pr);
      } else {
          __this.imgs[item.openid].onload = function() {
            //onload后直接跟函数 = = 这儿是个坑，当然后面做缓存都没有用了
          }
      }

      //名字
      context.font = 16 * __this.pr + "px PingFangSC-Regular";
      context.textAlign = "left";
      if (highlight) {
          context.fillStyle = "#E64F42";
      } else {
          context.fillStyle = "#A46F31";
      }
      if(__this.getStringLengthForChinese(item.nickname) > 4) {
          for(let i = 9;i > 3; i--) {
      		if (__this.getStringLengthForChinese(item.nickname.substr(0, i)) < 5) {
      			item.nickname = item.nickname.substr(0, i) + '...';
      			break;
      		}
      	}
      }
      context.fillText(item.nickname, pos.x + 108 * __this.pr, pos.y + 46 * __this.pr);

      //成绩

      context.font = 16 * __this.pr + "px PingFangSC-Regular";
      context.textAlign = "right";
      if (highlight) {
          context.fillStyle = "#E64F42";
      } else {
          context.fillStyle = "#A46F31";
      }
      context.fillText(item.KVDataList[0].value + 'm', pos.x + 233 * __this.pr, pos.y + 46 * __this.pr);

      //纹底
      if (highlight) {
          __this.fillRoundRect(context, pos.x - 4.5 * __this.pr, pos.y + 71 * __this.pr, 242 * __this.pr, 3 * __this.pr, 1.5 * __this.pr, '#E76C58');
      } else {
          __this.fillRoundRect(context, pos.x - 4.5 * __this.pr, pos.y + 71 * __this.pr, 242 * __this.pr, 3 * __this.pr, 1.5 * __this.pr, '#CBA164');
      }
      //context.fillRect(pos.x - 4.5, pos.y + 71, 242, 3);
      // 没有任何效果
      // let gradient = context.createLinearGradient(pos.x, pos.y + 71 * __this.pr, pos.x, pos.y + 74 * __this.pr);
      // gradient.addColorStop(0, 'rgba(255, 255, 255, 0)');
      // gradient.addColorStop(1, 'rgba(0, 0, 0, 255)');
      // context.fillStyle = gradient;
      // context.fillRect(pos.x - 4.5 * __this.pr, pos.y + 71 * __this.pr, 242 * __this.pr, 3 * __this.pr);

      context.restore();
  };

  /**该方法用来绘制一个有填充色的圆角矩形
  *@param cxt:canvas的上下文环境
  *@param x:左上角x轴坐标
  *@param y:左上角y轴坐标
  *@param width:矩形的宽度
  *@param height:矩形的高度
  *@param radius:圆的半径
  *@param fillColor:填充颜色
  **/
   __this.fillRoundRect = (cxt,x,y,width,height,radius,/*optional*/fillColor) => {
      //圆的直径必然要小于矩形的宽高
      if(2*radius>width || 2*radius>height){return false;}
      cxt.save();
      cxt.translate(x,y);
      //绘制圆角矩形的各个边
      __this.drawRoundRectPath(cxt,width,height,radius);
      cxt.fillStyle=fillColor||"#000";//若是给定了值就用给定的值否则给予默认值
      cxt.fill();
      cxt.restore();
  };

  /**该方法用来绘制圆角矩形
  *@param cxt:canvas的上下文环境
  *@param x:左上角x轴坐标
  *@param y:左上角y轴坐标
  *@param width:矩形的宽度
  *@param height:矩形的高度
  *@param radius:圆的半径
  *@param lineWidth:线条粗细
  *@param strokeColor:线条颜色
  **/
   __this.strokeRoundRect = (cxt,x,y,width,height,radius,/*optional*/lineWidth,/*optional*/strokeColor) => {
      //圆的直径必然要小于矩形的宽高
      if(2*radius>width || 2*radius>height){return false;}
      cxt.save();
      cxt.translate(x,y);
      //绘制圆角矩形的各个边
      __this.drawRoundRectPath(cxt,width,height,radius);
      cxt.lineWidth = lineWidth||2;//若是给定了值就用给定的值否则给予默认值2
      cxt.strokeStyle=strokeColor||"#000";
      cxt.stroke();
      cxt.restore();
  };

   __this.drawRoundRectPath = (cxt,width,height,radius) => {
      cxt.beginPath();
      //从右下角顺时针绘制，弧度从0到1/2PI
      cxt.arc(width-radius,height-radius,radius,0,Math.PI/2);
      //矩形下边线
      cxt.lineTo(radius,height);
      //左下角圆弧，弧度从1/2PI到PI
      cxt.arc(radius,height-radius,radius,Math.PI/2,Math.PI);
      //矩形左边线
      cxt.lineTo(0,radius);
      //左上角圆弧，弧度从PI到3/2PI
      cxt.arc(radius,radius,radius,Math.PI,Math.PI*3/2);
      //上边线
      cxt.lineTo(width-radius,0);
      //右上角圆弧
      cxt.arc(width-radius,radius,radius,Math.PI*3/2,Math.PI*2);
      //右边线
      cxt.lineTo(width,height-radius);
      cxt.closePath();
  };
  __this.getStringLengthForChinese = (val) => {
      var str = new String(val);
      var bytesCount = 0;
      for (var i = 0 ,n = str.length; i < n; i++) {
          var c = str.charCodeAt(i);
          if ((c >= 0x0001 && c <= 0x007e) || (0xff60<=c && c<=0xff9f)) {
              bytesCount += 1;
          } else {
              bytesCount += 2;
          }
      }
      return (bytesCount/2).toFixed(0);
  };
